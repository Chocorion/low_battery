#ifndef IMAGE_BANK_H
#define IMAGE_BANK_H

#include <SFML/Graphics.hpp>
#include <iostream>
#include <unordered_map>

using namespace std;

namespace game {

    class ImageBank {
        static const string _DATA;
        static const int _BLOC_SIZE;

        private:
            unordered_map<string, sf::Texture*> _bank;
            void load_texture(string pathName, string textureName, int abs, int ord, int width=1, int height=1);
            void load_bank();

            ImageBank();
            

        public:
            ~ImageBank();
            const sf::Texture* getImage(string imageName) const;

            static ImageBank& getInstance() {
                static ImageBank bank;
                return bank;
            }

            ImageBank(ImageBank const&)      = delete;
            void operator=(ImageBank const&) = delete;
    };

}
#endif