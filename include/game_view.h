#ifndef GAME_VIEW_H
#define GAME_VIEW_H

#include <SFML/Graphics.hpp>
#include "image_bank.h"

namespace game {
	class View {
		private:
			sf::RenderWindow& _window;
			ImageBank& _imageBank;

		public:
			View(sf::RenderWindow& window);
			void tick();
	};
}

#endif