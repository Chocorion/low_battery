#ifndef GAME_CONTROLLER_H
#define GAME_CONTROLLER_H

#include <SFML/Graphics.hpp>

namespace game {
	class Controller {
		private:
			sf::Event   _event;
			sf::RenderWindow& _window;
		public:
			Controller(sf::RenderWindow& window);

			bool tick();
	};
}

#endif