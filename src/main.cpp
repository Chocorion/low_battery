#include <iostream>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

#include "game_model.h"
#include "game_view.h"
#include "game_controller.h"

using namespace game;
int main()
{
    sf::RenderWindow window(sf::VideoMode(640, 480), "Low Battery...", sf::Style::Titlebar|sf::Style::Close);
    window.setVerticalSyncEnabled(true);
    window.setFramerateLimit(60);

    //Maybe I should change it later
    window.setKeyRepeatEnabled(false);
    


    Model model           = Model();
    View view             = View(window);
    Controller controller = Controller(window);
    

    // on fait tourner le programme jusqu'à ce que la fenêtre soit fermée
    while (controller.tick())
    {
        view.tick();
    }

    return 0;
}
