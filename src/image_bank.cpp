#include "image_bank.h"
#include <iostream>


using namespace std;

namespace game{
    string const ImageBank::_DATA = "../rsc/images/";
    int  const ImageBank::_BLOC_SIZE = 16;

    void ImageBank::load_texture(string pathName, string textureName, int abs, int ord, int width, int height) {
        sf::Texture* texture = new sf::Texture();

        texture->loadFromFile(
            pathName, 
            sf::IntRect(
                _BLOC_SIZE + abs * _BLOC_SIZE,  //Offset bloc in the tileset
                ord * _BLOC_SIZE,
                width * _BLOC_SIZE,
                height * _BLOC_SIZE
            )
        );

        _bank.insert({textureName, texture});
    }


    void ImageBank::load_bank() {
        load_texture(_DATA+"tileset.png", "tree_1", 0, 0, 3, 3);
        load_texture(_DATA+"tileset.png", "tree_2", 3, 0, 3, 3);
        load_texture(_DATA+"tileset.png", "tree_3", 6, 0, 3, 3);
        
        load_texture(_DATA+"tileset.png", "grass_1", 0, 20);
    }
    

    ImageBank::ImageBank() {
        cout << "Création de la banque d'image." << endl;
        load_bank();
    }

    ImageBank::~ImageBank() {
        cout << "Destruction de la banque d'image." << endl;
    }

    const sf::Texture* ImageBank::getImage(string imageName) const {
        if (_bank.find(imageName) == _bank.end()) {
            cerr << "Image " << imageName << " not found in the bank !" << endl;
            return nullptr;
        } else {
            return _bank.at(imageName);
        }
    }
}