#include "../include/game_controller.h"
#include "../include/game_view.h"
#include <iostream>

using namespace std;

namespace game {
	Controller::Controller(sf::RenderWindow& window): _window(window) {
		cout << "Création du controller." << endl;
	}

	bool Controller::tick() {
		while (_window.pollEvent(_event)) {
			if (_event.type == sf::Event::Closed) {
				return false;
			}

			if (_event.type == sf::Event::KeyPressed) {
				if (_event.key.code == sf::Keyboard::A) {
					cout << "You press A ! " << endl;
				}
			} else if (_event.type == sf::Event::KeyReleased) {
				if (_event.key.code == sf::Keyboard::A) {
					cout << "You release A !" << endl;
				}
			}
		}

		return true;
	}
}