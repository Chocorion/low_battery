#include "../include/game_view.h"
#include <iostream>

using namespace std;

namespace game {
	View::View(sf::RenderWindow &window) : _window(window), _imageBank(ImageBank::getInstance())
	{
		cout << "Création de la vue." << endl;
	}

	void View::tick() {
		_window.clear();


		sf::Sprite sprite;
		float scale = 1.6f;
		sprite.setScale(sf::Vector2f(scale, scale));

		//Scale ! TO DO

		for (int x = 0; x < 20; x++) {
			for (int y = 0; y < 20; y++) {
				sprite.setPosition(sf::Vector2f(x * 16 * scale, y * 16 * scale));
				sprite.setTexture(*_imageBank.getImage("grass_1"), true);
				_window.draw(sprite);

			}
		}

		sprite.setPosition(sf::Vector2f(0 * scale, 16 * scale));
		sprite.setTexture(*_imageBank.getImage("tree_1"), true);
		_window.draw(sprite);

		sprite.setPosition(sf::Vector2f(4*16 * scale, 0 * scale));
		sprite.setTexture(*_imageBank.getImage("tree_2"), true);
		_window.draw(sprite);

		sprite.setPosition(sf::Vector2f(13*16 * scale, 15*16 * scale));
		sprite.setTexture(*_imageBank.getImage("tree_3"), true);
		_window.draw(sprite);
		
		
		_window.display();
	}
}